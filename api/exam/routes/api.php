<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/login', [UserController::class, 'login'])->name('login_api');
Route::post('/register', [UserController::class, 'register'])->name('registration_api');


Route::middleware('auth:api')->group(function () {
    Route::apiResource('posts', PostController::class);
    Route::post('/logout', [UserController::class, 'logout']);
    Route::prefix('posts')->group(function () {
        Route::post('/{post_id}/comments', [CommentController::class, 'store']);
        Route::patch('/{id}/comments/{comment_id}', [CommentController::class, 'update']);
        Route::delete('/{id}/comments/{comment_id}', [CommentController::class, 'destroy']);
    });
});

Route::get('posts/{post_id}/comments', [CommentController::class, 'index']);
Route::get('posts', [PostController::class, 'index']);
Route::get('posts/{id}', [PostController::class, 'show']);
