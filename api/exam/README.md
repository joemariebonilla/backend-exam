

## API Installation

In order to successfully run the API, The following software is recommended:

- PHP 7+.
- MySQL 8.

First run the following.

    * Create a schema name it whatever you want.
    * cd api/exam
    * copy .env.example paste it same level directory and named it .env and put your DB credentials
    * composer install && php artisan key:generate && php artisan migrate && php artisan passport:install
    * php artisan serve

Any questions just let me know.
