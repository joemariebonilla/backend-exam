<?php

namespace App\Http\Controllers;

use App\Repository\CommentRepository;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * @var CommentRepository
     */
    private $comment_repo;

    /**
     * CommentController constructor.
     * @param CommentRepository $commentRepository
     */
    public function __construct(CommentRepository $commentRepository)
    {
        $this->comment_repo = $commentRepository;
    }

    /**
     * @param $post_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($post_id)
    {
        return $this->comment_repo->getAllCommentsByPost($post_id);
    }

    /**
     * @param Request $request
     * @param $post_id
     */
    public function store(Request $request, $post_id)
    {
        return $this->comment_repo->createComment($request, $post_id);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param $id
     * @return \App\Http\Resources\CommentResource|\Illuminate\Http\JsonResponse|object
     */
    public function update(Request $request, $id, $comment_id)
    {
        return $this->comment_repo->updateComment($request, $id, $comment_id);
    }

    /**
     * @param $comment_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, $comment_id)
    {
        return $this->comment_repo->deleteComment($id, $comment_id);
    }
}
