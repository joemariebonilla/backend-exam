<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\UserRepository;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $user_repo;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->user_repo = $userRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|object
     */
    public function login(Request $request)
    {
        return $this->user_repo->authenticate($request);
    }

    /**
     * @param Request $request
     * @return \App\Http\Resources\UserResource|\Illuminate\Http\JsonResponse|object
     */
    public function register(Request $request)
    {
        return $this->user_repo->registerUser($request);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        return $this->user_repo->logout();
    }
}
