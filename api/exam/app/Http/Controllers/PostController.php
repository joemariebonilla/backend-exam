<?php

namespace App\Http\Controllers;

use App\Repository\PostRepository;
use Illuminate\Http\Request;

class PostController extends Controller
{

    /**
     * @var PostRepository
     */
    private $post_repo;

    /**
     * PostController constructor.
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->post_repo = $postRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->post_repo->getAllPost();
    }

    /**
     * @param Request $request
     * @return \App\Http\Resources\PostResource|\Illuminate\Http\JsonResponse|object
     */
    public function store(Request $request)
    {
        return $this->post_repo->createPost($request);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->post_repo->getPostByID($id);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \App\Http\Resources\PostResource|\Illuminate\Http\JsonResponse|object
     */
    public function update(Request $request, $id)
    {
        return $this->post_repo->updatePost($request, $id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->post_repo->deletePost($id);
    }
}
