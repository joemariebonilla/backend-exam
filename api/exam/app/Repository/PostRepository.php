<?php

namespace App\Repository;

use App\Http\Resources\PostResource;
use App\Models\Post;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PostRepository
{

    /**
     * @return mixed
     */
    public function getAllPost()
    {
        return (new PostResource(Post::paginate()))->response();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPostByID($id)
    {
        try {
            $post = Post::findOrFail($id);
            return (new PostResource($post))->response();
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }

    }

    /**
     * @param $request
     * @return PostResource|\Illuminate\Http\JsonResponse|object
     */
    public function createPost($request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'string'],
            'content' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return (new ResponseRepository)->errorResponse($validator->errors());
        }

        DB::beginTransaction();
        try {
            $post = Post::create([
                'title' => $request->title,
                'content' => $request->content,
                'slug' => str_replace(' ', '-', strtolower($request->content)),
                'user_id' => auth()->id(),
            ]);
        } catch (Exception $e) {
            return (new ResponseRepository())->errorResponse([$e->getMessage()]);
        }
        DB::commit();
        return (new PostResource($post))->response();
    }

    /**
     * @param $request
     * @param $id
     * @return PostResource|\Illuminate\Http\JsonResponse|object
     */
    public function updatePost($request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['string'],
            'content' => ['string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return (new ResponseRepository)->errorResponse($validator->errors());
        }
        $objPost = Post::find($id);

        if (is_null($objPost)) {
            return (new ResponseRepository)->errorResponse(['post_id' => 'Post not found']);
        }

        DB::beginTransaction();
        try {

            if ($request->has('content')) {
                $content = $request->content;
                $slug = str_replace(' ', '-', strtolower($content));
            } else {
                $content = $objPost->content;
                $slug = $objPost->slug;
            }

            $objPost->forceFill([
                'title' => $request->title ?? $objPost->title,
                'content' => $content,
                'slug' => $slug,
            ])->save();

        } catch (Exception $e) {
            return (new ResponseRepository())->errorResponse([$e->getMessage()]);
        }
        DB::commit();
        return (new PostResource($objPost))->response();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePost($id)
    {
        Post::find($id)->delete();
        return response()->json(['status' => 'record deleted sucessfully']);
    }
}
