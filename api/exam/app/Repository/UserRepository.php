<?php

namespace App\Repository;

use App\Http\Resources\UserResource;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserRepository
{
    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse|object
     */
    public function authenticate($request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['email:rfc,dns', 'required'],
            'password' => ['required']
        ]);

        if ($validator->fails()) {
            return (new ResponseRepository)->errorResponse($validator->errors());
        }

        $credentials = $request->only('email', 'password');

        if (!auth()->attempt($credentials)) {

            return (new ResponseRepository)->errorResponse(['email' => ['These credentials do not match our records.']]);

        } else {
            $user = auth()->user();
            $objToken = $user->createToken('authToken');
            $access_token = $objToken->accessToken;
            $expiration = $objToken->token->expires_at;
            $success['token'] = $access_token;
            $success['token_type'] = 'bearer';
            $success['expires_at'] = $expiration;
            return response()->json($success);
        }
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse|object
     */
    public function registerUser($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'email' => ['required', 'email:rfc,dns', 'unique:users'],
            'password' => ['required', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return (new ResponseRepository)->errorResponse($validator->errors());
        }

        DB::beginTransaction();
        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
        } catch (Exception $e) {
            return (new ResponseRepository())->errorResponse([$e->getMessage()]);
        }
        DB::commit();
        return (new UserResource($user))->response();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'logout successfully']);
    }
}
