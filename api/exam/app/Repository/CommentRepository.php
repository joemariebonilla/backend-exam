<?php

namespace App\Repository;

use App\Http\Resources\CommentResource;
use App\Http\Resources\PostResource;
use App\Models\Comment;
use App\Models\Post;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CommentRepository
{
    /**
     * @param $post_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCommentsByPost($post_id)
    {
        try {
            $post = Post::findOrFail($post_id);
            return (new PostResource($post))->response();
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }
    }

    /**
     * @param $request
     * @param $post_id
     * @return CommentResource|\Illuminate\Http\JsonResponse|object
     */
    public function createComment($request, $post_id)
    {
        $validator = Validator::make($request->all(), [
            'body' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return (new ResponseRepository)->errorResponse($validator->errors());
        }

        DB::beginTransaction();
        try {
            $post = Post::find($post_id);
            $comment = $post->comments()->create([
                'body' => $request->body,
                'creator_id' => auth()->id(),
            ]);

        } catch (Exception $e) {
            return (new ResponseRepository())->errorResponse([$e->getMessage()]);
        }
        DB::commit();
        return (new CommentResource($comment))->response();
    }

    /**
     * @param $request
     * @param $id
     * @return CommentResource|\Illuminate\Http\JsonResponse|object
     */
    public function updateComment($request, $id, $comment_id)
    {
        $validator = Validator::make($request->all(), [
            'body' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return (new ResponseRepository)->errorResponse($validator->errors());
        }
        $objComment = Comment::find($comment_id);

        if (is_null($objComment)) {
            return (new ResponseRepository)->errorResponse(['comment_id' => 'Comment not found']);
        }

        DB::beginTransaction();
        try {

            $objComment->forceFill([
                'body' => $request->body,
            ])->save();

        } catch (Exception $e) {
            return (new ResponseRepository())->errorResponse([$e->getMessage()]);
        }
        DB::commit();
        return (new CommentResource($objComment))->response();
    }

    /**
     * @param $comment_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteComment($id, $comment_id)
    {
        Comment::find($comment_id)->delete();
        return response()->json(['status' => 'record deleted sucessfully']);
    }
}
